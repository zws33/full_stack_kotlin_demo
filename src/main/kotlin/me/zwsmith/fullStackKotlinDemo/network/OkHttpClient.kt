package me.zwsmith.fullStackKotlinDemo.network

import okhttp3.OkHttpClient

fun buildOkHttpClient(apiKeyInterceptor: ApiKeyInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
            .addInterceptor(apiKeyInterceptor)
            .build()
}