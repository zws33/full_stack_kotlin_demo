package me.zwsmith.fullStackKotlinDemo.network

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ApiKeyInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val originalUrl = request.url()

        val newUrl = originalUrl
                .newBuilder()
                .addQueryParameter("api_key", API_KEY)
                .build()

        val newRequest = request
                .newBuilder()
                .url(newUrl)
                .build()

        return chain.proceed(newRequest)
    }

    companion object {
        private val TAG = ApiKeyInterceptor::class.java.simpleName
        private const val API_KEY = "4bb7039eb3726d8db6db4b535bd17fec"
    }
}