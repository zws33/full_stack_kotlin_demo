package me.zwsmith.fullStackKotlinDemo

import me.zwsmith.fullStackKotlinDemo.network.ApiKeyInterceptor
import me.zwsmith.fullStackKotlinDemo.network.buildOkHttpClient
import me.zwsmith.fullStackKotlinDemo.services.MovieService
import me.zwsmith.fullStackKotlinDemo.services.buildMovieServiceImplementation
import okhttp3.OkHttpClient
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class FullStackKotlinDemoApplication {
    @Bean
    fun provideApiKeyInterceptor(): ApiKeyInterceptor = ApiKeyInterceptor()

    @Bean
    fun provideOkHttpClient(apiKeyInterceptor: ApiKeyInterceptor): OkHttpClient {
        return buildOkHttpClient(apiKeyInterceptor)
    }

    @Bean
    fun provideMovieService(okHttpClient: OkHttpClient): MovieService {
        return buildMovieServiceImplementation(okHttpClient)
    }

}

fun main(args: Array<String>) {
    runApplication<FullStackKotlinDemoApplication>(*args)
}
