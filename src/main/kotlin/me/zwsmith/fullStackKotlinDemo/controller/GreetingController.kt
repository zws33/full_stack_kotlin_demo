package me.zwsmith.fullStackKotlinDemo.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("greeting")
class GreetingController {
    @GetMapping
    fun sayHello(@RequestParam(value = "name", defaultValue = "World") name: String): String {
        return "Hello, $name!"
    }
}