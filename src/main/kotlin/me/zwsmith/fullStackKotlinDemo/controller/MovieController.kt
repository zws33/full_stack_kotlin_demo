package me.zwsmith.fullStackKotlinDemo.controller

import me.zwsmith.fullStackKotlinDemo.services.MovieService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("movie")
class MovieController(private val movieService: MovieService) {

    @GetMapping(value = ["popular"], produces = ["application/json"])
    fun popularMovies(@RequestParam(name = "page", required = false) page: Int?): String {
        return movieService.getPopularMovies(page ?: DEFAULT_PAGE).body()?.string() ?: "Error"
    }

    companion object {
        private const val DEFAULT_PAGE = 1
    }
}
