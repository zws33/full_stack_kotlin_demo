package me.zwsmith.fullStackKotlinDemo.services

import okhttp3.Response

interface MovieService {
    fun getPopularMovies(pageNumber: Int): Response

    companion object {
        private val TAG = MovieService::class.java.simpleName
    }
}