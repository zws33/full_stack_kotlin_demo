package me.zwsmith.fullStackKotlinDemo.services

import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

class MovieServiceImpl(private val okHttpClient: OkHttpClient) : MovieService {

    private fun buildPopularMoviesUrl(pageNumber: Int): HttpUrl {
        return HttpUrl.Builder()
                .scheme("https")
                .host(HOST)
                .addPathSegment(BASE_PATH)
                .addPathSegment(DISCOVER)
                .addPathSegment(MOVIE)
                .addQueryParameter("language", "en-US")
                .addQueryParameter("sort_by", "popularity.desc")
                .addQueryParameter("page", pageNumber.toString())
                .build()
    }

    override fun getPopularMovies(pageNumber: Int): Response {
        val url: HttpUrl = buildPopularMoviesUrl(pageNumber)

        val request = Request.Builder().url(url).build()

        return okHttpClient.newCall(request).execute()
    }

    companion object {
        private val TAG = MovieServiceImpl::class.java.simpleName
        private const val HOST = "api.themoviedb.org"
        private const val BASE_PATH = "3"
        private const val DISCOVER = "discover"
        private const val MOVIE = "movie"
    }
}