package me.zwsmith.fullStackKotlinDemo.services

import okhttp3.OkHttpClient

fun buildMovieServiceImplementation(okHttpClient: OkHttpClient) =
        MovieServiceImpl(okHttpClient = okHttpClient)